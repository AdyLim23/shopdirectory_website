<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $staffRole = Role::where('name', 'staff')->first();
        $merchantRole = Role::where('name', 'merchant')->first();
        
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin')
        ]);

        $staff = User::create([
            'name' => 'Staff',
            'email' => 'staff@staff.com',
            'password' => bcrypt('staff')
        ]);

        $tenant = User::create([
            'name' => 'Merchant',
            'email' => 'merchant@merchant.com',
            'password' => bcrypt('tenant')
        ]);

        $admin->roles()->attach($adminRole);
        $staff->roles()->attach($staffRole);
        $tenant->roles()->attach($merchantRole);
        
        //random create 5 users with role tenant
        factory(User::class, 5)->create()->each(function($user){
            $role = Role::where('name', 'merchant')->get();
            $user->roles()->attach($role);
        });
    }
}

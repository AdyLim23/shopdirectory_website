<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code',10)->index();
            $table->string('description',50)->index();
            $table->string('lotnumber', 10)->index();
            $table->char('zone', 2)->index();
            $table->char('floorlevel', 2)->index();
            $table->mediumText('image')->nullable();
            
            $table->unsignedBigInteger('category_id')->unsign()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('merchant_id')->unsign()->nullable();
            $table->foreign('merchant_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}

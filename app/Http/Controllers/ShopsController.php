<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Shop;
use App\User;

class ShopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $categories = [];

        if(Gate::allows('merchant-only', $user)) {
            $ownedShops = Shop::where('merchant_id', $user->id)->get();
            foreach($ownedShops as $shop) {
                $category = $shop->category()->get();
                array_push($categories, $category);
            }
            return view('shops.index')->with('categories', $categories);
        }

        $shops = Shop::all();
        foreach($shops as $shop) {
            $category = $shop->category()->get();
            array_push($categories, $category);
        }
        return view('shops.index')->with('categories', $categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shops = Shop::where('category_id', $id)->orderBy('name')->get();
        $category = DB::table('categories')->where('id', $id)->get()->pluck('name');
        return view('shops.show')->with('shops', $shops)->with('category', $category);
    }

    public function edit($id)
    {
        $user = Auth::user();
        if(Gate::allows('merchant-only', $user)) {
            $shop = Shop::find($id);
            if($shop->merchant_id == $user->id) {
                return view('shops.edit')->with('shop', $shop);
            }
            return redirect()->route('login')->with('warning', 'You cannot edit others people shop');
        }
        return redirect()->route('login')->with('warning', 'Please login as Merchant');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'description' => 'nullable|max:50',
            'lotnumber' => 'required|max:10',
            'zone' => 'required',
            'floorlevel' => 'required',
            'category' => 'required',
            'shop_image' => 'image|nullable|max:1999'
        ]);

        // Handle file upload
        if($request->hasFile('shop_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('shop_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('shop_image')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('shop_image')->storeAs('public/shop_images', $filenameToStore);
        } else {
            $filenameToStore = 'no-image-available.png';
        }

        // Create Post
        $shop = Shop::find($id);
        $shop->name = $request->input('name');
        $shop->description = $request->input('description');
        $shop->lotnumber = $request->input('lotnumber');
        $shop->zone = $request->input('zone');
        $shop->floorlevel = $request->input('floorlevel');
        $shop->category_id = $request->input('category');
        $shop->merchant_id = auth()->user()->id;
        if($request->hasFile('shop_image')) {
            $shop->image = $filenameToStore;
        }
        $shop->save();

        return redirect('/shops')->with('success', 'Shop Info Updated');
    }

    public function search(Request $request)
    {
        $search = $request->input('search');

        return view('welcome',compact('search'));
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href='/shops' class='btn btn-default'>Go Back</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">{{ implode(', ', $category->toArray()) }}</div>
                </div>
                <div class="panel-body">
                    @if($shops->count() > 0)
                        @php
                            $i = 0
                        @endphp
                        @foreach($shops as $shop)
                        <div class="col-xs-6">
                            <div>
                                <div style="font-size:20px">{{ ++$i }}. {{ $shop->name }}</div>
                                <img style="width:100%" src="/storage/shop_images/{{$shop->image}}" alt="responsive-image">
                            </div>
                        </div>
                        <div id={{ $shop->id }} class="col-xs-6">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <h4>{{ $shop->name }}'s Details</h4>
                                    </tr>
                                    <tr>
                                        <th>Lot No.</th>
                                        <td>{{ $shop->lotnumber }}</td>
                                    </tr>
                                    <tr>
                                        <th>Zone</th>
                                        @if($shop->zone == '0')
                                            <td>New Wing</td>
                                        @else
                                            <td>Old Wing</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Level</th>
                                        <td>{{ $shop->floorlevel }}</td>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <td>{{ $shop->description }}</td>
                                    </tr>
                                    @can('merchant-only', Auth::user())
                                    <tr>
                                        <th>Action</th>
                                        <td>
                                            <a href="{{ route('shops.edit', $shop->id) }}" class="col-xs-3">
                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                            </a>    
                                        </td>
                                    </tr>
                                    @endcan
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12"><hr></div>
                        @endforeach
                    @else
                        <div>No shop found in "{{ implode(', ', $category->toArray()) }}"</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
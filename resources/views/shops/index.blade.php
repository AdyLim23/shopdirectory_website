@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-handing">
                    <h2 class="text-center">Shop Categories</h2>
                </div>
                <div class="panel-body text-center">
                    @foreach($categories as $category)
                    <a class="col-lg-3 col-md-4 col-sm-6" href="{{ route('shops.show', implode(', ', $category->pluck('id')->toArray())) }}" style="margin-top:20px">
                        <button type="button" class="btn btn-default text-center">
                        <img style="width:225px; height:225px" src="/storage/category_images/{{ implode(', ', $category->pluck('name')->toArray()) }}.png" alt="responsive-image">
                            <div>{{ implode(', ', $category->pluck('name')->toArray()) }}</div>
                        </button>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
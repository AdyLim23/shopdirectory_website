<!doctype html>
<?php 

use App\Shop;
use App\Category;
     
     $shops = Shop::with(['category'])->get();

    if(empty($search)){
        $shops = Shop::with(['category'])->get();        
    }else{
        $find = Shop::all()->where('name',$search);        

    }
?>
<html >
    <head>
      
        <title>UTAR SE MALL</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: white;
                font-family: 'Raleway', sans-serif;
                height: 100%;
                margin: 0;
            }

            .bg { 
                background-image: url("/storage/layout_images/slider.jpg");
                height: 100%; 
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: whitesmoke;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height bg">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">UTAR SE MALL</div>

                <h1 class="m-b-md">The Most Affortable, Agreeable, Enjoyable as Always</h1>

                <div class="links">
                    <a href="/shops">Shop Categories</a>
                    <a href="#">About</a>
                    <a href="#">Contact Us</a>
                </div>
                <div class="row d-flex justify-content-center" style="padding-top:10%;">
                    <div class="col-md-10">
                        <form action="{{action('ShopsController@search')}}" method="get">
                            {{csrf_field()}}
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input type="text" name="search" placeholder="Eg:KFC" class="btn-group1" style="width:80%;font-size:130%;">
                                <button type="submit" class="btn-form" style="font-size:130%;"><span class="icon-magnifier search-icon"></span>SEARCH<i class="pe-7s-angle-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
   
         <section class="main-block">
            <div class="container">
                <div>
                    @if(empty($search))
                        <p style="color:black;font-weight:700;"><b>Shop Lists</b></p> 
                        <table class="table table-striped task-table" style="color:black;font-weight:700;">
                            <thead style="color:black;font-weight:700;">
                                <tr>
                                    <th>No.</th>
                                    <th>Shop Name</th>
                                    <th>Lot Number</th>
                                    <th>Zone</th>
                                    <th>Floor Level</th>
                                    <th>Category</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($shops as $i => $shop)
                                <tr>
                                    <td class="table-text">
                                        <div>{{ $i+1 }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>
                                            {!! link_to_route(
                                                'shops.show',
                                                $title = $shop->name,
                                                $parameters = [
                                                    'id' => $shop->category->id,
                                                ]
                                                ) !!}
                                            </div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $shop->lotnumber }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $shop->zone }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $shop->floorlevel }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ Category::pluck('name','id')[$shop->category_id] }}</div>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                        <div style="padding-bottom:10%;">
                        <p style="color:black;font-weight:700;"><b>Search Result</b></p>
                        <table class="table table-striped task-table" style="color:black;font-weight:700;">
                            <!-- Table Headings -->
                            <thead style="color:black;font-weight:700;">
                                <tr>
                                    <th>No.</th>
                                    <th>Shop Name</th>
                                    <th>Lot Number</th>
                                    <th>Zone</th>
                                    <th>Floor Level</th>
                                    <th>Category</th>
                                </tr>
                            </thead>

                            <!-- Table Body -->
                            <tbody>
                                @foreach ($find as $i => $f)
                                <tr>
                                    <td class="table-text">
                                        <div>{{ $i+1 }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>
                                            {!! link_to_route(
                                                'shops.show',
                                                $title = $f->name,
                                                $parameters = [
                                                    'id' => $f->category->id,
                                                ]
                                                ) !!}
                                            </div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $f->lotnumber }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $f->zone }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $f->floorlevel }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ Category::pluck('name','id')[$f->category_id] }}</div>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        @endif
                        </div>
                    </div>
                </section>
    </body>
</html>
@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Shop Information (Admin-only)</div>
                <div class="panel-body">
                    {{ Form::open(['action' => ['Admin\ShopController@update', $shop->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <div class="form-group">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name', $shop->name, ['class' => 'form-control', 'placeholder' => 'Shop Name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('merchant_id', 'Merchant ID')}}
                            {{Form::text('merchant_id', $shop->merchant_id, ['class' => 'form-control', 'placeholder' => 'User ID'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('code', 'Code')}}
                            {{Form::text('code', $shop->code, ['class' => 'form-control', 'placeholder' => 'Code'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('lotnumber', 'Lot Number')}}
                            {{Form::text('lotnumber', $shop->lotnumber, ['class' => 'form-control', 'placeholder' => 'Lot Number'])}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('zone', 'Zone') }}
                            {{ Form::select('zone', array('0' => 'New Wing', '1' => 'Old Wing'), $shop->zone, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('floorlevel', 'Floor Level') }}
                            {{ Form::select('floorlevel', array('G' => 'G', 'F1' => 'F1', 'F2' => 'F2', 'F3' => 'F3'), $shop->floorlevel, 
                            ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('category', 'Category') }}
                            {{ Form::select('category', array(
                                '1' => 'Amenities', '2' => 'Anchors',  '3' => 'Beauty and Healthcare', 
                                '4' => 'Books', '5' => 'Convenience Store', '6' => 'Digital Lifestyle and Photography',
                                '7' => 'Foods and Beverages', '8' => 'Footwear', '9' => 'Fitness',
                                '10' => 'Leisure and Entertainment', '11' => 'Shirts and Fashion', '12' => 'Haircare/Salon',
                                '13' => 'Hobbies', '14' => 'Others'), $shop->category_id, ['class' => 'form-control']) }}
                        </div>
                        <div class='form-group'>
                            {{ Form::label('description', 'Description') }}
                            {{ Form::textarea('description', $shop->description, ['class' => 'form-control', 'style' => 'resize:none']) }}
                        </div>
                        <div class='form-group'>
                            {{ Form::label('shop_image', 'Image') }}
                            {{ Form::file('shop_image') }}
                        </div>
                        {{ Form::hidden('_method', 'PUT') }}
                        {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
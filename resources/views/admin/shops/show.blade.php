@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href='/admin/shops' class='btn btn-default'>Go Back</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Shop Details</div>
                </div>
                <div class="panel-body">
                    <img class="col-md-6" style="width:50%" src="/storage/shop_images/{{$shop->image}}" alt="responsive-image">
                    <div class="col-md-6">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $shop->name }}</td>
                                </tr>
                                <tr>
                                    <th>Code</th>
                                    <td>{{ $shop->code }}</td>
                                </tr>
                                <tr>
                                    <th>Lot Number</th>
                                    <td>{{ $shop->lotnumber }}</td>
                                </tr>
                                <tr>
                                    <th>Zone</th>
                                    @if($shop->zone == '0')
                                        <td>New Wing</td>
                                    @else
                                        <td>Old Wing</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Floor Level</th>
                                    <td>{{ $shop->floorlevel }}</td>
                                </tr>
                                <tr>
                                    <th>Category</th>
                                    <td>{{ implode(', ', $shop->getCategory($shop->category_id)->toArray()) }}</td>
                                </tr>
                                <tr>
                                    <th>Merchant Name</th>
                                    <td>{{ implode(', ', $shop->getOwner($shop->merchant_id)->toArray()) }}</td>
                                </tr>
                                <tr>
                                    <th>Merchant ID</th>
                                    <td>{{ $shop->merchant_id }}</td>
                                </tr>
                                <tr>
                                    <th>Actions</th>
                                    <td>
                                        <a href="{{ route('admin.shops.edit', $shop->id) }}" class="col-xs-3">
                                            <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                        </a>
                                        <form action="{{ route('admin.shops.destroy', $shop->id) }}" method="POST" class="col-xs-3">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection